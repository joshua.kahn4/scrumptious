from django.shortcuts import render, get_object_or_404
from django.http import HttpRequest
from recipes.models import Recipe
from recipes.forms import RecipeForm

def list_recipes(request: HttpRequest):
       recipes = Recipe.objects.all()
       context = {
              "recipes": recipes
       }
       return render(request, "recipes/list.html", context)

def edit_recipe(request:HttpRequest):
    recipe: Recipe = get_object_or_404(Recipe, id=id)
    if request.method == "GET":
           form = RecipeForm(instance=recipe)
    elif request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
                       form.save()
    context: dict[str, RecipeForm] = {
        "form": form,
        "recipe": recipe # type: ignore
    }
    print(context)
    return render(request, "recipes/create.html", context)

def create_recipe(request: HttpRequest):
        if request.method == "POST":
                form = RecipeForm(request.POST)
                if form.is_valid():
                       form.save()
        else:
            form = RecipeForm()
        context: dict[str, RecipeForm] = {
                "form": form
        }
        print(context)
        return render(request, "recipes/create.html", context)

# Create your views here.
def show_recipe(request: HttpRequest, id: int):
        """
        GET /recipes/<int:id>

        Returns a recipe page
        """
        recipe: Recipe = get_object_or_404(Recipe, id=id)
        return render(request, "recipes/detail.html", {
                "recipe": recipe
        })
