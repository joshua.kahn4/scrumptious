from django.urls import path
from recipes.views import show_recipe, create_recipe, edit_recipe, list_recipes

urlpatterns = [
    path("recipes/", list_recipes, name="list_recipes"),
    path("recipes/<int:id>/", show_recipe, name="show_recipe"),
    path("recipes/<int:id>/edit", edit_recipe, name="edit_recipe"),
    path("recipes/create/", create_recipe, name="create_recipe")
    ]
